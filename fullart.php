<?php
define("title", "Home Page") ;
include "public.php";
session_start();
?>
<!doctype html>
<html>
<head>
    
    
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Testing PHP</title>


<link rel="stylesheet" href="css/reset.css" type="text/css" />
<link rel="stylesheet" href="css/styles.css" type="text/css" />
<link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">

<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/slider.js"></script>
<script type="text/javascript" src="js/superfish.js"></script>

<script type="text/javascript" src="js/custom.js"></script>

<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0" />

<!--
woodply, a free CSS web template by ZyPOP (zypopwebtemplates.com/)

Download: http://zypopwebtemplates.com/

License: Creative Commons Attribution
//-->
</head>
<body>
<div id="container">

    <header> 
	<div class="width">

    		<h1><a href="index.php"><?php  echo $websiteName       ?></a></h1>

		<nav>
	
    			<ul class="sf-menu dropdown">

			
        			<li class="selected"><a href="index.php"><i class="fa fa-home"></i> Home</a></li>

            			<li>

					<a href="examples.html"><i class="fa fa-code"></i> Style examples</a>
            			
					<ul>
                				<li><a href="three-column.html">Three Column</a></li>
						<li><a href="one-column.html">One Column</a></li>
                    				<li><a href="text.html">Text page</a></li>
                			</ul>

            			</li>

            
				<li>

					<a href="#"><i class="fa fa-database"></i> Products</a>
            				
					<ul>
                				<li><a href="#">Product One</a></li>
                   				<li><a href="#">Product Two</a></li>
                   				<li><a href="#">Product Three</a></li>
                			</ul>

            			</li>
            
				<li><a href="#"><i class="fa fa-phone"></i> Contact</a></li>
       			</ul>

			
			<div class="clear"></div>
    		</nav>
       	</div>

	<div class="clear"></div>

       
    </header>


    <div id="intro">

	<div class="width">
      
		<div class="intro-content">
	
                    <h2>Testing Php Methods Here</h2>
                    <p>Nothing But Whats Said At Top</p>
                                  	
			<p> <a href="reg.php" class="button button-slider">
                    <i class="fa fa-info"></i>  Go To Register Form</a>

                <a href="login.php" class="button button-slider">
                    <i class="fa fa-info"></i>  Go To Login Form</a>

                <a href="weatherform.php" class="button button-reversed button-slider">
                                <i class="fa fa-info"></i>  Go To Weather API Test</a>
            </p>
                    

            	</div>
                
            </div>
            

	</div>

    <div id="body" class="width">



		<section id="content" class="two-column with-right-sidebar">
            <?php
            $conn = getConnection() ;
            $id = intval($_GET['id']);
            $result=mysqli_query ($conn,"select`id`,`title`,`subject`,`longtxt`,`created` from `article` where id=$id");
            while($row=mysqli_fetch_assoc($result)) {

                $title = ('' . $row['title'] . "");
                $subject = ('' . $row['subject'] . "");
                $lngtxt = ('' . $row['longtxt'] . "");
                $created = ('' . $row['created'] . "");

            }
                echo <<<eof

                <table>
  <tr>
    <th><h2>$title</h2></th>
  </tr>
  <tr>
    <td><h3>$subject</h3></td>
  </tr>
  <tr>
    <td><p>$lngtxt</p></td>
  </tr>
  <tr>
    <td>
    <h4>$created<h4>
    </td>
  </tr>
</table>

eof;

            ?>
        </section>

        <aside class="sidebar big-sidebar right-sidebar">

	
            <ul>	
               <li>
                    <h4>Blocklist</h4>
                    <ul class="blocklist">
                        <li><a class="selected" href="index.html">Home Page</a></li>
                        <li><a href="examples.html">Style Examples</a>
				<ul>
					<li><a href="three-column.html">Three Column</a></li>
					<li><a href="one-column.html">One column / no sidebar</a></li>
					<li><a href="text.html">Text / left sidebar</a></li>
				</ul>
			</li>
                        <li><a href="three-column.html">Three column layout example</a></li>
                        <li><a href="#">Sed aliquam libero ut velit bibendum</a></li>
                        <li><a href="#">Maecenas condimentum velit vitae</a></li>
                    </ul>
                </li>
                
               

		
               
                <li>
			<h4>News</h4>
			<ul class="newslist">
				<li>
					<p><span class="newslist-date">Jul 21</span>
			                   Quisque hendrerit lorem sit amet dui viverra dictum. Phasellus imperdiet magna sit amet arcu tristique ultricies ut in dui.</p>
				</li>

<li>
					<p><span class="newslist-date">May 09</span>
			                   Mauris et felis semper, congue dui ac, iaculis ipsum. Fusce non rhoncus risus, quis luctus nisl. Donec vitae velit tincidunt, tincidunt felis eu, suscipit nibh. </p>
 
				</li>
			</ul>
                </li>
	
   		<li>
                    <h4>Maecenas varius</h4>
                    <ul>
				<li><a href="#">Nam cursus nisi nec viverra iaculis</a></li>
				<li><a href="#">Integer lacinia risus id nibh vestibulum</a></li>
				<li><a href="#">Mauris eget ante ut elit rutrum ornare </a></li>
				<li><a href="#">Vivamus quis orci et suscipit consequa</a></li>
				<li><a href="#">Nam eget tellus adipiscin hendrerit</a></li>
			</ul>
                </li>
                
            </ul>
		
        </aside>
    	<div class="clear"></div>
    </div>
    <footer>
        <div class="footer-content width">
            <ul>
            	<li><h4>Proin accumsan</h4></li>
                <li><a href="#">Rutrum nulla a ultrices</a></li>
                <li><a href="#">Blandit elementum</a></li>
                <li><a href="#">Proin placerat accumsan</a></li>
                <li><a href="#">Morbi hendrerit libero </a></li>
                <li><a href="#">Curabitur sit amet tellus</a></li>
            </ul>
            
            <ul>
            	<li><h4>Condimentum</h4></li>
                <li><a href="#">Curabitur sit amet tellus</a></li>
                <li><a href="#">Morbi hendrerit libero </a></li>
                <li><a href="#">Proin placerat accumsan</a></li>
                <li><a href="#">Rutrum nulla a ultrices</a></li>
                <li><a href="#">Cras dictum</a></li>
            </ul>

 	    <ul>
                <li><h4>Suspendisse</h4></li>
                <li><a href="#">Morbi hendrerit libero </a></li>
                <li><a href="#">Proin placerat accumsan</a></li>
                <li><a href="#">Rutrum nulla a ultrices</a></li>
                <li><a href="#">Curabitur sit amet tellus</a></li>
                <li><a href="#">Donec in ligula nisl.</a></li>
            </ul>
            
            <ul class="endfooter">
            	<li><h4>Suspendisse</h4></li>
                <li>Integer mattis blandit turpis, quis rutrum est. Maecenas quis arcu vel felis lobortis iaculis fringilla at ligula. Nunc dignissim porttitor dolor eget porta. <br /><br />

<div class="social-icons">

<a href="#"><i class="fa fa-facebook fa-2x"></i></a>

<a href="#"><i class="fa fa-twitter fa-2x"></i></a>

<a href="#"><i class="fa fa-youtube fa-2x"></i></a>

<a href="#"><i class="fa fa-instagram fa-2x"></i></a>

</div>

</li>
            </ul>
            
            <div class="clear"></div>
        </div>
        <div class="footer-bottom">
            <p>&copy; YourSite 2014. <a href="http://zypopwebtemplates.com/">Free HTML5 Templates</a> by ZyPOP</p>
         </div>
    </footer>
</div>
</body>
</html>