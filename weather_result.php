<?php

error_reporting(0);
include "public.php";

?>

<!doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>woodply - Free CSS Template by ZyPOP</title>


<link rel="stylesheet" href="css/reset.css" type="text/css" />
<link rel="stylesheet" href="css/styles.css" type="text/css" />
<link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">


<!--[if lt IE 9]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/slider.js"></script>
<script type="text/javascript" src="js/superfish.js"></script>

<script type="text/javascript" src="js/custom.js"></script>

<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0" />

<!--
woodply, a free CSS web template by ZyPOP (zypopwebtemplates.com/)

Download: http://zypopwebtemplates.com/

License: Creative Commons Attribution
//-->
</head>
<body>
<div id="container">

    <header> 
	<div class="width">
    		<h1><a href="index.php"><strong><?php  echo $websiteName ?></strong></a></h1>

		<nav>
	
    			<ul class="sf-menu dropdown">

			
        			<li><a href="index.php"><i class="fa fa-home"></i> Home</a></li>

            			<li class="selected">

					<a href="examples.html"><i class="fa fa-code"></i> Style examples</a>
            			
					<ul>
                				<li><a href="three-column.html">Three Column</a></li>
						<li><a href="one-column.html">One Column</a></li>
                    				<li><a href="text.html">Text page</a></li>
                			</ul>

            			</li>

	     			
            
				<li>

					<a href="#"><i class="fa fa-database"></i> Products</a>
            				
					<ul>
                				<li><a href="#">Product One</a></li>
                   				<li><a href="#">Product Two</a></li>
                   				<li><a href="#">Product Three</a></li>
                			</ul>

            			</li>
            
				<li><a href="#"><i class="fa fa-phone"></i> Contact</a></li>


       			</ul>

			
			<div class="clear"></div>
    		</nav>
       	</div>

	<div class="clear"></div>

       
    </header>


    <div id="body" class="width">

	<section id="content" class="one-column">

        <br><br><br><br><br><br><br><br><br><br>

        <?php

        $job = $_REQUEST['job'];

        if( $job=="get_weather") {

            $val = $_REQUEST['val'];

            if (empty($val))
                $val = 'location';


            $content = file_get_contents("https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22tehran%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys");

            $jsonArray = json_decode($content, true);


            if ($val == 'location') {
                $Loc_country = $jsonArray['query']['results']['channel']['location']['country'];

                $Loc_city = $jsonArray['query']['results']['channel']['location']['city'];


                echo "کشور : $Loc_country <br><br>";

                echo "شهر : $Loc_city ";
            }

            else if ($val == 'wind'){

                $wind_chill = $jsonArray['query']['results']['channel']['wind']['chill'];

                $wind_dir = $jsonArray['query']['results']['channel']['wind']['direction'];

                $wind_speed = $jsonArray['query']['results']['channel']['wind']['speed'];

                echo "سرمای باد : $wind_chill <br><br>";

                echo "جهت باد : $wind_dir <br><br>";

                echo "سرعت باد : $wind_speed <br><br>";


            }

            else if ($val == 'atmo'){

                $atmo_hum = $jsonArray['query']['results']['channel']['atmosphere']['humidity'];

                $atmo_press = $jsonArray['query']['results']['channel']['atmosphere']['pressure'];

                $atmo_visi = $jsonArray['query']['results']['channel']['atmosphere']['visibility'];

                echo "رطوبت هوا : $atmo_hum <br><br>";

                echo "فشار هوا : $atmo_press <br><br>";

                echo "برد دید : $atmo_visi <br><br>";


            }

            else if ($val == 'astro'){

                $astro_rise = $jsonArray['query']['results']['channel']['astronomy']['sunrise'];

                $astro_set = $jsonArray['query']['results']['channel']['astronomy']['sunset'];

                echo "طلوع خورشید : $astro_rise <br><br>";

                echo "غروب خورشید : $astro_set <br><br>";


            }

            else if ($val == 'forcast'){

                $forcast_form = $jsonArray['query']['results']['channel']['item']['condition']['text'];

                $forcast_temp = $jsonArray['query']['results']['channel']['item']['condition']['temp'];

                echo "وضعیت آب و هوا : $forcast_form <br><br>";

                echo "دمای هوا به فارن هایت : $forcast_temp fahrenheit <br><br>";

            }
        }


        ?>

        <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>

        </section>
        
        
    	<div class="clear"></div>
    </div>
    <footer>
        <div class="footer-content width">
            <ul>
            	<li><h4>Proin accumsan</h4></li>
                <li><a href="#">Rutrum nulla a ultrices</a></li>
                <li><a href="#">Blandit elementum</a></li>
                <li><a href="#">Proin placerat accumsan</a></li>
                <li><a href="#">Morbi hendrerit libero </a></li>
                <li><a href="#">Curabitur sit amet tellus</a></li>
            </ul>
            
            <ul>
            	<li><h4>Condimentum</h4></li>
                <li><a href="#">Curabitur sit amet tellus</a></li>
                <li><a href="#">Morbi hendrerit libero </a></li>
                <li><a href="#">Proin placerat accumsan</a></li>
                <li><a href="#">Rutrum nulla a ultrices</a></li>
                <li><a href="#">Cras dictum</a></li>
            </ul>

 	    <ul>
                <li><h4>Suspendisse</h4></li>
                <li><a href="#">Morbi hendrerit libero </a></li>
                <li><a href="#">Proin placerat accumsan</a></li>
                <li><a href="#">Rutrum nulla a ultrices</a></li>
                <li><a href="#">Curabitur sit amet tellus</a></li>
                <li><a href="#">Donec in ligula nisl.</a></li>
            </ul>
            
            <ul class="endfooter">
            	<li><h4>Suspendisse</h4></li>
                <li>Integer mattis blandit turpis, quis rutrum est. Maecenas quis arcu vel felis lobortis iaculis fringilla at ligula. Nunc dignissim porttitor dolor eget porta. <br /><br />

<div class="social-icons">

<a href="#"><i class="fa fa-facebook fa-2x"></i></a>

<a href="#"><i class="fa fa-twitter fa-2x"></i></a>

<a href="#"><i class="fa fa-youtube fa-2x"></i></a>

<a href="#"><i class="fa fa-instagram fa-2x"></i></a>

</div>

</li>
            </ul>
            
            <div class="clear"></div>
        </div>
        <div class="footer-bottom">
            <p>&copy; YourSite 2014. <a href="http://zypopwebtemplates.com/">Free HTML5 Templates</a> by ZyPOP</p>
         </div>
    </footer>
</div>
</body>
</html>