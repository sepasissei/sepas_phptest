<?php

error_reporting(0);

$job = $_REQUEST['job'];

if( $job=="get_weather") {

    $val = $_REQUEST['val'];

    if (empty($val))
        $val = 'location';


    $content = file_get_contents("https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22tehran%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys");

    $jsonArray = json_decode($content, true);


    if ($val == 'location') {
        $Loc_country = $jsonArray['query']['results']['channel']['location']['country'];

        $Loc_city = $jsonArray['query']['results']['channel']['location']['city'];


        echo "کشور : $Loc_country <br><br>";

        echo "شهر : $Loc_city ";
    }

    else if ($val == 'wind'){

        $wind_chill = $jsonArray['query']['results']['channel']['wind']['chill'];

        $wind_dir = $jsonArray['query']['results']['channel']['wind']['direction'];

        $wind_speed = $jsonArray['query']['results']['channel']['wind']['speed'];

        echo "سرمای باد : $wind_chill <br><br>";

        echo "جهت باد : $wind_dir <br><br>";

        echo "سرعت باد : $wind_speed <br><br>";


    }

    else if ($val == 'atmo'){

        $atmo_hum = $jsonArray['query']['results']['channel']['atmosphere']['humidity'];

        $atmo_press = $jsonArray['query']['results']['channel']['atmosphere']['pressure'];

        $atmo_visi = $jsonArray['query']['results']['channel']['atmosphere']['visibility'];

        echo "رطوبت هوا : $atmo_hum <br><br>";

        echo "فشار هوا : $atmo_press <br><br>";

        echo "برد دید : $atmo_visi <br><br>";


    }

    else if ($val == 'astro'){

        $astro_rise = $jsonArray['query']['results']['channel']['astronomy']['sunrise'];

        $astro_set = $jsonArray['query']['results']['channel']['astronomy']['sunset'];

        echo "طلوع خورشید : $astro_rise <br><br>";

        echo "غروب خورشید : $astro_set <br><br>";


    }

    else if ($val == 'forcast'){

        $forcast_form = $jsonArray['query']['results']['channel']['item']['condition']['text'];

        $forcast_temp = $jsonArray['query']['results']['channel']['item']['condition']['temp'];

        echo "وضعیت آب و هوا : $forcast_form <br><br>";

        echo "دمای هوا به فارن هایت : $forcast_temp fahrenheit <br><br>";

    }
}


?>